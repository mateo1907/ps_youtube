<div id="topOfYT">

</div>
<div data-type="youtube" data-video-id="{$list->items[0]->id->videoId}"></div>
<div id="yt_videos">
  {foreach from=$list->items item=item }
    {if $item->id->videoId}
      <div class="one_video text-center">
        <img data-id-video="{$item->id->videoId}" class="yt_video" src="{$item->snippet->thumbnails->medium->url}" alt="">
        <p >{$item->snippet->title}</p>
      </div>
    {/if}

  {/foreach}
</div>
<div class="pull-right">
  <i id="back" class="fa fa-arrow-circle-left" aria-hidden="true"></i>
  <i id="forward" class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
