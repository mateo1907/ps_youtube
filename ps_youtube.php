<?php
if (!defined('_PS_VERSION_')) {
  exit;
}

class PS_Youtube extends Module
{
  public function __construct()
  {
    $this->name = 'ps_youtube';
    $this->tab = 'front_office_features';
    $this->version = '1.0';
    $this->author = 'Mateusz Prasał';
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('YouTube Videos on CMS Page');
    $this->description = $this->l('Show all your YouTube videos on one of CMS Pages');
    $this->confirmUninstall = $this->l('Are you sure you want to unistall?');

  }

  public function install()
{
  if (!parent::install() || !Configuration::updateValue('PSYoutube','PSYoutube') || !$this->registerHook('header')) {
    return false;
  }

  return true;
}

public function getContent()
{
  $output = '';
  if (Tools::isSubmit('submit'.$this->name)) {
    $apikey = Tools::getValue('PSYoutube_APIKEY');
    $id = Tools::getValue('PSYoutube_ID');

    if (!$apikey || !Validate::isCleanHtml($apikey)) {
      $output .= $this->displayError($this->l('Nieprawidłowy klucz API'));
    } else if (!$id || !Validate::isCleanHtml($apikey)) {
      $output .= $this->displayError($this->l('Nieprawidłowy ID'));
    } else {
      Configuration::updateValue('PSYoutube_APIKEY',Tools::getValue('PSYoutube_APIKEY'));
      Configuration::updateValue('PSYoutube_ID',Tools::getValue('PSYoutube_ID'));

      $output .= $this->displayConfirmation($this->l('Wszystko w porządku'));
    }


  }
  return $output . $this->displayForm();
}

public function displayForm()
{
    // Get default language
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    // Init Fields form array
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('Klucz API Youtube'),
                'name' => 'PSYoutube_APIKEY',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Identyfikator kanału'),
                'name' => 'PSYoutube_ID',
                'size' => 20,
                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
    );

    $helper = new HelperForm();

    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;

    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );

    // Load current value
    $helper->fields_value = array(
      'PSYoutube_APIKEY' => Configuration::get('PSYoutube_APIKEY'),
      'PSYoutube_ID' => Configuration::get('PSYoutube_ID'),

    );

    return $helper->generateForm($fields_form);
}

  public function hookHeader($params)
  {
    $this->context->controller->addCSS($this->_path . 'views/css/font-awesome.min.css');
    if (Tools::getValue('controller') == 'cms') {
      $this->context->controller->addJquery();
      $this->context->controller->addJS('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js');
      $this->context->controller->addCSS('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css');
      $this->context->controller->addCSS('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css');

      $this->context->controller->addJS($this->_path . 'views/js/plyr.js');
      $this->context->controller->addCSS($this->_path . 'views/css/plyr.css');

      $this->context->controller->addCSS($this->_path . 'views/css/main.css');
      $this->context->controller->addJS($this->_path . 'views/js/main.js');


    }

  }
}
