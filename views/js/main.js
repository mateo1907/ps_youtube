jQuery(document).ready(function($){
  var player = plyr.setup()

  $('.one_video').on('click',function(){
    $('.one_video_active').removeClass('one_video_active')
    $(this).addClass('one_video_active');
    $('html,body').animate({
        scrollTop: $('#topOfYT').offset().top },
        'slow');
  })
  if (player != false) {
    player.forEach(function(instance){
      $('.yt_video').on('click',function(){
        var id = $(this).attr('data-id-video')
        instance.source({
          type:       'video',
          title:      '',
          sources: [{
              src:    id,
              type:   'youtube'
          }]
        });
      })
    })
  }


  $('#yt_videos').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  arrows:true,
  prevArrow: $("#back"),
  nextArrow: $("#forward"),
});
})
