<?php

class CmsController extends CmsControllerCore
{
  public function initContent()
  {

      parent::initContent();
      $parent_cat = new CMSCategory(1, $this->context->language->id);
      $this->context->smarty->assign('id_current_lang', $this->context->language->id);
      $this->context->smarty->assign('home_title', $parent_cat->name);
      $this->context->smarty->assign('cgv_id', Configuration::get('PS_CONDITIONS_CMS_ID'));

      if ($this->assignCase == 1) {
          if (isset($this->cms->id_cms_category) && $this->cms->id_cms_category) {
              $path = Tools::getFullPath($this->cms->id_cms_category, $this->cms->meta_title, 'CMS');
          } elseif (isset($this->cms_category->meta_title)) {
              $path = Tools::getFullPath(1, $this->cms_category->meta_title, 'CMS');
          }

          $this->cms->content = $this->returnContent($this->cms->content);

          $this->context->smarty->assign(array(
              'cms' => $this->cms,
              'content_only' => (int)Tools::getValue('content_only'),
              'path' => $path,
              'body_classes' => array($this->php_self.'-'.$this->cms->id, $this->php_self.'-'.$this->cms->link_rewrite)
          ));

          if ($this->cms->indexation == 0) {
              $this->context->smarty->assign('nobots', true);
          }
      } elseif ($this->assignCase == 2) {
          $this->context->smarty->assign(array(
              'category' => $this->cms_category, //for backward compatibility
              'cms_category' => $this->cms_category,
              'sub_category' => $this->cms_category->getSubCategories($this->context->language->id),
              'cms_pages' => CMS::getCMSPages($this->context->language->id, (int)$this->cms_category->id, true, (int)$this->context->shop->id),
              'path' => ($this->cms_category->id !== 1) ? Tools::getPath($this->cms_category->id, $this->cms_category->name, false, 'CMS') : '',
              'body_classes' => array($this->php_self.'-'.$this->cms_category->id, $this->php_self.'-'.$this->cms_category->link_rewrite)
          ));
      }

      $this->setTemplate(_PS_THEME_DIR_.'cms.tpl');

  }

  public function returnContent($content)
  {
    $apiKey = Configuration::get('PSYoutube_APIKEY');
    $apiID = Configuration::get('PSYoutube_ID');
    $numberOfVideos = Configuration::get('PSYoutube_NumberOfVideos');

    $videoList = json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/search?key={$apiKey}&channelId={$apiID}&part=snippet,id&order=date&maxResults=50"));

    $this->context->smarty->assign(array(
      'list' => $videoList,
      'numberOfVideos' => $numberOfVideos
    ));
    $output = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'ps_youtube/ps_youtube.tpl');

    $var = '{youtube}';
    $content = str_replace($var, $output, $content);

    return $content;
  }
}
